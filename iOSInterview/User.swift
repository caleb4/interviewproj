//
//  User.swift
//  iOSInterview
//
//  Created by Caleb McGuire on 11/1/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var username: String = ""
    dynamic var email: String = ""
    dynamic var address: Address?
    dynamic var phone: String = ""
    dynamic var website: String = ""
    dynamic var company: Company?
    dynamic var userThumbnail: Data?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


//{
//    "id": 1,
//    "name": "Leanne Graham",
//    "username": "Bret",
//    "email": "Sincere@april.biz",
//    "address": {
//        "street": "Kulas Light",
//        "suite": "Apt. 556",
//        "city": "Gwenborough",
//        "zipcode": "92998-3874",
//        "geo": {
//            "lat": "-37.3159",
//            "lng": "81.1496"
//        }
//    },
//    "phone": "1-770-736-8031 x56442",
//    "website": "hildegard.org",
//    "company": {
//        "name": "Romaguera-Crona",
//        "catchPhrase": "Multi-layered client-server neural-net",
//        "bs": "harness real-time e-markets"
//    }
//}

//
//  ViewController.swift
//  iOSInterview
//
//  Created by Caleb McGuire on 11/1/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import SVProgressHUD

class ViewController: UITableViewController {

    var userArray: [User] = []
    var realm: Realm!
    var thumbnailUrls: [String] = []
    let networkManager = NetworkReachabilityManager()
    var detailVC: UserDetailViewController!
    var userCount: Int {
        return userArray.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setUserData()
        setupUI()
        checkNetwork()
    }
    
    func setUserData(){
        self.realm = try! Realm()
        let users = self.realm.objects(User.self)
        
        // Cell is set if there is no connection on initial launch / no data has been added to realm yet.
        if users.count == 0 {
            let noConnection = User()
            noConnection.name = "No Data Found..."
            noConnection.username = "Check your internet connection."
            self.userArray.append(noConnection)
        } else {
            self.userArray = Array(users)
        }
    }
    
    func setupUI(){
        title = "User List"
    }
    
    func checkNetwork(){
        // Listens for network status changes - on reachable, calls request for thumbnails.
        self.networkManager?.listener = { status in
            print("Network Status Changed: \(status)")
            print("network reachable \(String(describing: self.networkManager?.isReachable))")
            if (self.networkManager?.isReachable)! {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.show(withStatus: "Connected! Updating user list...")
                //if there is a connection - add refreshControl.
                self.refreshControl = UIRefreshControl()
                self.refreshControl?.attributedTitle = NSAttributedString(string: "Refreshing User List...")
                self.refreshControl?.addTarget(self, action: #selector(self.requestThumbnails), for: UIControlEvents.valueChanged)
                self.requestThumbnails()
            } else {
                self.refreshControl = nil
            }
        }
        self.networkManager?.startListening()
    }
    
    func requestUsers(){
        Alamofire.request("https://jsonplaceholder.typicode.com/users").responseJSON { response in
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result)   // result of response serialization
            
            if let result = response.result.value as? [AnyObject] {
                var count = 0
                for user in result {
                    let newUser = User(value: user)
                    
                    //Just going to add the first thumbnail for the first user and so forth.
                    let urlText = self.thumbnailUrls[count]
                    if let url = URL(string: urlText) {
                        if let imgData = try? Data(contentsOf: url) {
                            newUser.userThumbnail = imgData
                        }
                    }
                   
                    try! self.realm.write {
                        self.realm.add(newUser, update: true)
                    }
                    count += 1
                }
                let users = self.realm.objects(User.self)
                self.userArray = Array(users)
                //Done with the thumbnail urls at this point.
                self.thumbnailUrls = []
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
                self.refreshControl?.endRefreshing()
            } else {
                SVProgressHUD.dismiss()
                self.refreshControl?.endRefreshing()
                self.showDataFailAlert()
            }
        }
    }

    func requestThumbnails(){
        Alamofire.request("https://jsonplaceholder.typicode.com/photos").responseJSON { response in
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result)   // result of response serialization
            
            if let result = response.result.value as? [NSDictionary] {
                for url in result {
                    let thumbnailUrl = url["thumbnailUrl"] as! String
                    print(thumbnailUrl)
                    self.thumbnailUrls.append(thumbnailUrl)
//                    Once x users are retrieved - we only want x number of thumbnails.
                    if self.thumbnailUrls.count > self.userCount && self.userCount > 1 {
                        break
                    }
                }
                //got thumbnail urls - now get users.
                self.requestUsers()
            } else {
                SVProgressHUD.dismiss()
                self.refreshControl?.endRefreshing()
                self.showDataFailAlert()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "usercell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "usercell")
        
        let item = self.userArray[indexPath.row]
        if item.userThumbnail != nil {
            let thumbnail = UIImage(data: item.userThumbnail!)
            cell.imageView?.image = thumbnail
        }
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.username
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.detailVC = UserDetailViewController()
        self.detailVC.user = self.userArray[indexPath.row]
        self.navigationController?.pushViewController(self.detailVC, animated: true)
    }
    
    func showDataFailAlert(){
        print("an error occurred")
        let alert = UIAlertController(title: "Error", message: "Failed to fetch new data - Try Again?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            if self.thumbnailUrls.count > 0 {
                self.thumbnailUrls = []
            }
            SVProgressHUD.show(withStatus: "Updating user list...")
            self.requestThumbnails()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}


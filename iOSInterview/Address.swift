//
//  Address.swift
//  iOSInterview
//
//  Created by Caleb McGuire on 11/1/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import Foundation
import RealmSwift

class Address: Object {
    dynamic var street: String = ""
    dynamic var suite: String = ""
    dynamic var city: String = ""
    dynamic var zipcode: String = ""
    dynamic var geo: GeoPoint?
}


//{
//        "street": "Kulas Light",
//        "suite": "Apt. 556",
//        "city": "Gwenborough",
//        "zipcode": "92998-3874",
//        "geo": {
//            "lat": "-37.3159",
//            "lng": "81.1496"
//        }
//    }

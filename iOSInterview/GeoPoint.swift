//
//  GeoPoint.swift
//  iOSInterview
//
//  Created by Caleb McGuire on 11/1/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import Foundation
import RealmSwift

class GeoPoint: Object {
    dynamic var lat: String = ""
    dynamic var lng: String = ""
}

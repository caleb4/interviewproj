//
//  UserDetailViewController.swift
//  iOSInterview
//
//  Created by Caleb McGuire on 11/2/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import UIKit
import SnapKit
import MapKit

class UserDetailViewController: UIViewController, MKMapViewDelegate {
    
    var user: User!
    var scrollView = UIScrollView()
    var idLabel = UILabel()
    var nameLabel = UILabel()
    var userNameLabel = UILabel()
    var emailLabel = UILabel()
    var addressLabel = UILabel()
    var phoneLabel = UILabel()
    var websiteLabel = UILabel()
    var companyLabel = UILabel()
    let mapView = MKMapView()
    let regionRadius: CLLocationDistance = 10000
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //print(user.debugDescription)
        setupUI()
        setConstraints()
        if self.user.address?.geo != nil {
             dropPin()
        }
    }
    
    override func viewWillLayoutSubviews() {
        if UIDevice.current.orientation.isLandscape {
            self.mapView.snp.updateConstraints { (make) -> Void in
                make.top.equalTo(self.scrollView)
            }
        } else {
            self.mapView.snp.updateConstraints { (make) -> Void in
                make.top.equalTo(self.scrollView)
            }
        }
        self.view.updateConstraints()
    }
    
    func setupUI(){
        self.view.backgroundColor = UIColor.white
        self.scrollView.frame = self.view.bounds
        self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: 603)
        self.scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(self.scrollView)
        self.mapView.delegate = self
        self.scrollView.addSubview(self.mapView)
        self.idLabel.text = "Id: \(self.user.id)"
        self.scrollView.addSubview(self.idLabel)
        self.nameLabel.text = "Name: \(self.user.name)"
        self.scrollView.addSubview(self.nameLabel)
        self.userNameLabel.text = "Username: \(self.user.username)"
        self.scrollView.addSubview(self.userNameLabel)
        self.emailLabel.text = "Email: \(self.user.email)"
        self.scrollView.addSubview(self.emailLabel)
        self.addressLabel.numberOfLines = 0
        
        if let userAddress = self.user.address {
            self.addressLabel.text = "Address:\n\(userAddress.street)\n\(userAddress.suite)\n\(userAddress.city)\n\(userAddress.zipcode)"
        } else {
            self.addressLabel.text = "Address:\nNo Data"
        }
        self.scrollView.addSubview(self.addressLabel)
        self.phoneLabel.text = "Phone: \(self.user.phone)"
        self.scrollView.addSubview(self.phoneLabel)
        self.websiteLabel.text = "Website: \(self.user.website)"
        self.scrollView.addSubview(self.websiteLabel)
        self.companyLabel.numberOfLines = 0
        if let userCompany = self.user.company {
            self.companyLabel.text = "Company:\n\(userCompany.name)\n\(userCompany.catchPhrase)\n\(userCompany.bs)"
        } else {
            self.companyLabel.text = "Company:\nNo Data"
        }
        
        self.scrollView.addSubview(self.companyLabel)
        
    }
    
    func setConstraints(){
        
        self.mapView.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.scrollView)
            make.height.equalTo(self.scrollView).dividedBy(3.3)
            make.centerX.equalTo(self.scrollView)
        }
        self.idLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.mapView.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.nameLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.idLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.userNameLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.nameLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.emailLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.userNameLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.addressLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.emailLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.phoneLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.addressLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.websiteLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.phoneLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
        self.companyLabel.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.scrollView)
            make.top.equalTo(self.websiteLabel.snp.bottom).offset(11)
            make.left.equalTo(self.scrollView).offset(20)
        }
    }
    
    func dropPin(){
        guard let lat = Double(self.user.address!.geo!.lat), let long = Double(self.user.address!.geo!.lng) else {
            return
        }
        let location = CLLocation(latitude: lat, longitude: long)
        let pinCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        centerMapOnLocation(location)
        let pin = MKPointAnnotation()
        pin.coordinate = pinCoordinate
        pin.title = "\(self.user.username)'s Coords"
        pin.subtitle = self.user.address?.city
        self.mapView.addAnnotation(pin)
    }
    
    func centerMapOnLocation(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  self.regionRadius * 2.0, self.regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView != nil {
            annotationView!.annotation = annotation
        } else {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            
            //Sets the thumbnail to the callout.
            let imageViewSize = CGSize(width: 50, height: 50)
            let imageViewFrame = CGRect(origin: annotationView!.frame.origin, size: imageViewSize)
            if let thumbnailData = self.user.userThumbnail {
                let thumbnail = UIImage(data: thumbnailData)
                let thumbnailView = UIImageView.init(frame: imageViewFrame)
                thumbnailView.image = thumbnail
                thumbnailView.contentMode = .scaleAspectFill
                annotationView?.leftCalloutAccessoryView = thumbnailView as UIView
            }
        }
        return annotationView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//
//  Company.swift
//  iOSInterview
//
//  Created by Caleb McGuire on 11/1/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import Foundation
import RealmSwift

class Company: Object {
    dynamic var name: String = ""
    dynamic var catchPhrase: String = ""
    dynamic var bs: String = ""
}

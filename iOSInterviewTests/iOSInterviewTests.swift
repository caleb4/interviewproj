//
//  iOSInterviewTests.swift
//  iOSInterviewTests
//
//  Created by Caleb McGuire on 11/1/16.
//  Copyright © 2016 Caleb McGuire. All rights reserved.
//

import XCTest
@testable import iOSInterview
import Alamofire

class iOSInterviewTests: XCTestCase {
    
    var vc: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        vc = ViewController()
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //ViewController Tests
    func testSetUserData(){
        vc.setUserData()
        XCTAssert(vc.userArray.count > 1)
    }
    
    func testSetupUI(){
        vc.setupUI()
        XCTAssert(vc.title == "User List")
    }
    
    func testCheckNetwork(){
        vc.checkNetwork()
        XCTAssert(vc.networkManager?.networkReachabilityStatus != nil)
    }
    
    func testRequestThumbnails(){
        let ex = expectation(description: "longRunningFunction")
        Alamofire.request("https://jsonplaceholder.typicode.com/photos").responseJSON { response in
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result)   // result of response serialization
            
            if let result = response.result.value as? [NSDictionary] {
                for url in result {
                    let thumbnailUrl = url["thumbnailUrl"] as! String
                    print(thumbnailUrl)
                    self.vc.thumbnailUrls.append(thumbnailUrl)
                }
                //got thumbnail urls - now get users.
                ex.fulfill()
            } else {
                ex.fulfill()
            }
        }
        
        self.waitForExpectations(timeout: 4) { result in
            XCTAssert(self.vc.thumbnailUrls.count > 0)
        }
    }
    
    func testRequestUsers(){
        let exp = expectation(description: "longRunningFunction")
        vc.setUserData()
        Alamofire.request("https://jsonplaceholder.typicode.com/users").responseJSON { response in
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result)   // result of response serialization
            
            if let result = response.result.value as? [AnyObject] {
                var count = 0
                for user in result {
                    let newUser = User(value: user)
                    
                    try! self.vc.realm.write {
                        self.vc.realm.add(newUser, update: true)
                    }
                    count += 1
                }
                let users = self.vc.realm.objects(User.self)
                self.vc.userArray = Array(users)
                //Done with the thumbnail urls at this point.
                self.vc.thumbnailUrls = []
                self.vc.tableView.reloadData()
                self.vc.refreshControl?.endRefreshing()
                exp.fulfill()
            } else {
                exp.fulfill()
            }
        }
        self.waitForExpectations(timeout: 5) { result in
            XCTAssert(self.vc.userArray.count > 0)
        }
    }
    
    func testShowDataFailAlert(){
        vc.showDataFailAlert()
        print(vc.presentedViewController.debugDescription)
        XCTAssert(vc.presentedViewController is UIAlertController)
    }
    
    func testDidSelectRow(){
        let detailVC = UserDetailViewController()
        detailVC.user = vc.userArray[3]
        vc.navigationController?.pushViewController(detailVC, animated: true)
        detailVC.setupUI()
        
        XCTAssert(detailVC.companyLabel.text == "Company:\n\(detailVC.user.company!.name)\n\(detailVC.user.company!.catchPhrase)\n\(detailVC.user.company!.bs)")
    }
    
    func testSetConstraints(){
        let detailVC = UserDetailViewController()
        detailVC.user = vc.userArray[3]
        vc.navigationController?.pushViewController(detailVC, animated: true)
        detailVC.setupUI()
        detailVC.setConstraints()
        detailVC.view.setNeedsLayout()
        detailVC.view.layoutIfNeeded()
        
        XCTAssert(detailVC.companyLabel.bounds.width == detailVC.scrollView.bounds.width)
    }
    
    func testDropPin(){
        let detailVC = UserDetailViewController()
        detailVC.user = vc.userArray[3]
        vc.navigationController?.pushViewController(detailVC, animated: true)
        detailVC.dropPin()
        XCTAssert(detailVC.mapView.annotations.count > 0)
    }
    
    func testAnnotationView(){
        let detailVC = UserDetailViewController()
        detailVC.user = vc.userArray[3]
        vc.navigationController?.pushViewController(detailVC, animated: true)
        detailVC.dropPin()
        let title = detailVC.mapView.annotations[0].title!!
        XCTAssert(title == "\(detailVC.user.username)'s Coords")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}

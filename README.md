# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
> Interview assignment - This project uses Realm mobile database (realm.io) to store data offline and Alamofire library to make requests and monitor connection status. The user data is refreshed when the network status becomes reachable. The ui is programmatic and constraints are set using SnapKit (landscape and portrait modes fully supported). The map shows the user coordinates and tapping the pin will provide additional details. To incorporate the photos endpoint - the thumbnail urls are collected and the first n are set to the first n users. There is also a pull to refresh control added when the network is reachable.

* Areas of Improvement
> Possible improvements - More unit tests? Also should test with different user data set. UI is fairly standard at the moment as well and could be more colorful.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
> Open folder and open iOSInterview.xcworkspace with Xcode. For best performance and more accurate network testing - test on actual hardware.
* Configuration
* Dependencies
> Pods -
>>* Alamofire
>>* RealmSwift
>>* SVProgressHud
>>* SnapKit
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact